version_info = (1, 1, 7)

version = '.'.join(map(str, version_info))
__version__ = version
